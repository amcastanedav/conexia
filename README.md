# Conexia

Hi Everyone this was a little servlet created for technical test to apply to Conexia

## Getting Started

You need a mysql installation, java and maven

### Prerequisites

Create the schema of the database with the script you will find in resources.

Compile the project

```
mvn package
```

## Running the tests

```
mvn test
```

## Running the project

Change the properties of you database in the applications.properties file that you will find in resources and then

```
java -jar conexia-1.0.jar
```

## Using the project

You can either use the rest API or the frontend disposed to create and view objects and the reports

The REST API for creations can be explorer with the postman collection you will find in the project

The graphic way to create objects is in:
- /create/client
- /create/chef
- /create/waiter
- /create/table
- /create/invoice
- /create/detail

The reports can be found in
- /report/waiters
- /report/clients

And to view the object you have created you can use:
- /view/client?id=1
- /view/chef?id=1
- /view/waiter?id=1
- /view/table?id=1
- /view/invoice?id=1
- /view/detail?id=1

