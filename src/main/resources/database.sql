CREATE TABLE IF NOT EXISTS clients (
  id_client INT AUTO_INCREMENT,
  first_name VARCHAR(255) NOT NULL,
  last_name1 VARCHAR(255) NOT NULL,
  last_name2 VARCHAR(255) NOT NULL,
  observations TEXT,
  PRIMARY KEY (id_client)
)  ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS tables (
  id_table INT AUTO_INCREMENT,
  max_num INT,
  location TEXT,
  PRIMARY KEY (id_table)
)  ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS waiters (
  id_waiter INT AUTO_INCREMENT,
  first_name VARCHAR(255) NOT NULL,
  last_name1 VARCHAR(255) NOT NULL,
  last_name2 VARCHAR(255) NOT NULL,
  PRIMARY KEY (id_waiter)
)  ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS chefs (
  id_chef INT AUTO_INCREMENT,
  first_name VARCHAR(255) NOT NULL,
  last_name1 VARCHAR(255) NOT NULL,
  last_name2 VARCHAR(255) NOT NULL,
  PRIMARY KEY (id_chef)
)  ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS invoices (
  id_invoice INT AUTO_INCREMENT,
  id_client INT,
  id_waiter INT,
  id_table INT,
  date_invoice DATE,
  PRIMARY KEY (id_invoice),
  CONSTRAINT FK_invoice_client FOREIGN KEY (id_client) REFERENCES clients(id_client) ON DELETE NO ACTION,
  CONSTRAINT FK_invoice_waiter FOREIGN KEY (id_waiter) REFERENCES waiters(id_waiter) ON DELETE NO ACTION,
  CONSTRAINT FK_invoice_table FOREIGN KEY (id_table) REFERENCES tables(id_table) ON DELETE NO ACTION
)  ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS details (
  id_detail INT AUTO_INCREMENT,
  id_invoice INT,
  id_chef INT,
  dish VARCHAR(255) NOT NULL,
  cost INT,
  PRIMARY KEY (id_detail),
  CONSTRAINT FK_detalle_invoice FOREIGN KEY (id_invoice) REFERENCES invoices(id_invoice) ON DELETE NO ACTION,
  CONSTRAINT FK_detalle_chef FOREIGN KEY (id_chef) REFERENCES chefs(id_chef) ON DELETE NO ACTION
) ENGINE=INNODB;