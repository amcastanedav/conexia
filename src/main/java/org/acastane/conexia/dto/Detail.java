package org.acastane.conexia.dto;

/**
 * Created by angelica on 30/03/19.
 */
public class Detail {

    private Integer id_invoice;

    private Integer id_chef;

    private String dish;

    private Integer cost;

    public Integer getId_invoice() {
        return id_invoice;
    }

    public void setId_invoice(Integer id_invoice) {
        this.id_invoice = id_invoice;
    }

    public Integer getId_chef() {
        return id_chef;
    }

    public void setId_chef(Integer id_chef) {
        this.id_chef = id_chef;
    }

    public String getDish() {
        return dish;
    }

    public void setDish(String dish) {
        this.dish = dish;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }
}
