package org.acastane.conexia.dto;

/**
 * Created by angelica on 30/03/19.
 */
public class Invoice {

    private Integer id_client;

    private Integer id_waiter;

    private Integer id_table;

    public Integer getId_client() {
        return id_client;
    }

    public void setId_client(Integer id_client) {
        this.id_client = id_client;
    }

    public Integer getId_waiter() {
        return id_waiter;
    }

    public void setId_waiter(Integer id_waiter) {
        this.id_waiter = id_waiter;
    }

    public Integer getId_table() {
        return id_table;
    }

    public void setId_table(Integer id_table) {
        this.id_table = id_table;
    }
}
