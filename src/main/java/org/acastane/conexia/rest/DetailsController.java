package org.acastane.conexia.rest;

import org.acastane.conexia.core.Details;
import org.acastane.conexia.exceptions.CouldNotCreateDetailsException;
import org.acastane.conexia.service.ConexiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

/**
 * Created by angelica on 29/03/19.
 */
@RestController
@RequestMapping("/details")
public class DetailsController {

    @Autowired
    private ConexiaService conexiaService;

    @GetMapping("/{id}")
    ResponseEntity<String> getDetails(@PathVariable Integer id){
        Optional<Details> response = conexiaService.getDetails(id);
        if (response.isPresent()){
            return new ResponseEntity<>(response.get().toString(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(Responses.notFound, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/create")
    ResponseEntity<String> createDetail(@RequestBody Map<String, Object> payload){
        try {
            Integer id_invoice = Integer.valueOf(payload.get("id_invoice").toString());
            Integer id_chef = Integer.valueOf(payload.get("id_chef").toString());
            String dish = payload.get("dish").toString();
            Integer cost = Integer.valueOf(payload.get("cost").toString());
            conexiaService.saveDetails(id_invoice, id_chef, dish, cost);
            return new ResponseEntity<>(Responses.created, HttpStatus.OK);
        } catch (NullPointerException | NumberFormatException e) {
            return new ResponseEntity<>(Responses.badRequest, HttpStatus.BAD_REQUEST);
        } catch (CouldNotCreateDetailsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }
}
