package org.acastane.conexia.rest;

import org.acastane.conexia.core.Invoices;
import org.acastane.conexia.exceptions.CouldNotCreateInvoicesException;
import org.acastane.conexia.service.ConexiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

/**
 * Created by angelica on 29/03/19.
 */
@RestController
@RequestMapping("/invoices")
public class InvoiceController {

    @Autowired
    private ConexiaService conexiaService;

    @GetMapping("/{id}")
    ResponseEntity<String> getInvoice(@PathVariable Integer id){
        Optional<Invoices> response = conexiaService.getInvoices(id);
        if (response.isPresent()){
            return new ResponseEntity<>(response.get().toString(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(Responses.notFound, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/create")
    ResponseEntity<String> createInvoice(@RequestBody Map<String, Object> payload){
        try {
            Integer id_client = Integer.valueOf(payload.get("id_client").toString());
            Integer id_waiter = Integer.valueOf(payload.get("id_waiter").toString());
            Integer id_table = Integer.valueOf(payload.get("id_table").toString());
            conexiaService.saveInvoices(id_client, id_waiter, id_table);
            return new ResponseEntity<>(Responses.created, HttpStatus.OK);
        } catch (NullPointerException | NumberFormatException e) {
            return new ResponseEntity<>(Responses.badRequest, HttpStatus.BAD_REQUEST);
        } catch (CouldNotCreateInvoicesException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }
}
