package org.acastane.conexia.rest;

import org.acastane.conexia.core.Clients;
import org.acastane.conexia.service.ConexiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

/**
 * Created by angelica on 29/03/19.
 */
@RestController
@RequestMapping("/clients")
public class ClientsController {

    @Autowired
    private ConexiaService conexiaService;

    @GetMapping("/{id}")
    ResponseEntity<String> getClients(@PathVariable Integer id){
        Optional<Clients> response = conexiaService.getClients(id);
        if (response.isPresent()){
            return new ResponseEntity<>(response.get().toString(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(Responses.notFound, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/create")
    ResponseEntity<String> createClients(@RequestBody Map<String, Object> payload){
        try {
            String first_name = payload.get("first_name").toString();
            String last_name1 = payload.get("last_name1").toString();
            String last_name2 = payload.get("last_name2").toString();
            String observations = payload.get("observations").toString();
            Clients object = new Clients(first_name, last_name1, last_name2, observations);
            conexiaService.saveClient(object);
            return new ResponseEntity<>(Responses.created, HttpStatus.OK);
        } catch (NullPointerException e) {
            return new ResponseEntity<>(Responses.badRequest, HttpStatus.BAD_REQUEST);
        }
    }
}
