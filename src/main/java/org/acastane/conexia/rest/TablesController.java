package org.acastane.conexia.rest;

import org.acastane.conexia.core.Tables;
import org.acastane.conexia.service.ConexiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

/**
 * Created by angelica on 29/03/19.
 */
@RestController
@RequestMapping("/tables")
public class TablesController {

    @Autowired
    private ConexiaService conexiaService;

    @GetMapping("/{id}")
    ResponseEntity<String> getTables(@PathVariable Integer id){
        Optional<Tables> response = conexiaService.getTables(id);
        if (response.isPresent()){
            return new ResponseEntity<>(response.get().toString(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(Responses.notFound, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/create")
    ResponseEntity<String> createTables(@RequestBody Map<String, Object> payload){
        try {
            Integer max_num = Integer.valueOf(payload.get("max_num").toString());
            String location = payload.get("location").toString();
            Tables object = new Tables(max_num, location);
            conexiaService.saveTables(object);
            return new ResponseEntity<>(Responses.created, HttpStatus.OK);
        } catch (NullPointerException | NumberFormatException e) {
            return new ResponseEntity<>(Responses.badRequest, HttpStatus.BAD_REQUEST);
        }
    }
}
