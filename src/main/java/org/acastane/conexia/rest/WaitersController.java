package org.acastane.conexia.rest;

import org.acastane.conexia.core.Waiters;
import org.acastane.conexia.service.ConexiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

/**
 * Created by angelica on 29/03/19.
 */
@RestController
@RequestMapping("/waiters")
public class WaitersController {

    @Autowired
    private ConexiaService conexiaService;

    @GetMapping("/{id}")
    ResponseEntity<String> getWaiters(@PathVariable Integer id){
        Optional<Waiters> response = conexiaService.getWaiters(id);
        if (response.isPresent()){
            return new ResponseEntity<>(response.get().toString(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(Responses.notFound, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/create")
    ResponseEntity<String> createWaiters(@RequestBody Map<String, Object> payload){
        try {
            String first_name = payload.get("first_name").toString();
            String last_name1 = payload.get("last_name1").toString();
            String last_name2 = payload.get("last_name2").toString();
            Waiters object = new Waiters(first_name, last_name1, last_name2);
            conexiaService.saveWaiters(object);
            return new ResponseEntity<>(Responses.created, HttpStatus.OK);
        } catch (NullPointerException e) {
            return new ResponseEntity<>(Responses.badRequest, HttpStatus.BAD_REQUEST);
        }
    }
}
