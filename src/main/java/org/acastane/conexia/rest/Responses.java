package org.acastane.conexia.rest;

/**
 * Created by angelica on 29/03/19.
 */
public final class Responses {

    public static String created = "Object created";
    public static String notFound = "Object not found";
    public static String badRequest = "Bad request";
}
