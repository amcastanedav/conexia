package org.acastane.conexia.rest;

import org.acastane.conexia.service.ConexiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


/**
 * Created by angelica on 29/03/19.
 */
@RestController
@RequestMapping("/reports")
public class ReportsController {

    @Autowired
    private ConexiaService conexiaService;

    @GetMapping("/waiters/{num_months}")
    ResponseEntity<Object> getReportByWaiters(@PathVariable Integer num_months){
        return new ResponseEntity<>(conexiaService.buildReportByWaiters(num_months), HttpStatus.OK);
    }

    @GetMapping("/clients/{consumption}")
    ResponseEntity<Object> getClientsWithConsumption(@PathVariable Integer consumption){
        return new ResponseEntity<>(conexiaService.buildReportByClients(consumption), HttpStatus.OK);
    }
}
