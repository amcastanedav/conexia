package org.acastane.conexia.repository;

import org.acastane.conexia.core.Waiters;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by angelica on 29/03/19.
 */
public interface WaitersRepository extends CrudRepository<Waiters, Integer> {
}
