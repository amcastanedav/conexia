package org.acastane.conexia.repository;

import org.acastane.conexia.core.Chefs;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by angelica on 29/03/19.
 */
public interface ChefsRepository extends CrudRepository<Chefs, Integer> {
}
