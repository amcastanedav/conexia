package org.acastane.conexia.repository;

import org.acastane.conexia.core.Clients;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by angelica on 29/03/19.
 */
public interface ClientsRepository extends CrudRepository<Clients, Integer> {
}
