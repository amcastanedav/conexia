package org.acastane.conexia.repository;

import org.acastane.conexia.core.Invoices;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;

/**
 * Created by angelica on 29/03/19.
 */
public interface InvoicesRepository extends CrudRepository<Invoices, Integer>{

    @Query("SELECT coalesce(sum(cost), 0) FROM Details WHERE id_invoice IN " +
            "(SELECT id_invoice FROM Invoices WHERE id_waiter = :id_waiter AND date_invoice " +
            "BETWEEN :start_date AND :end_date)")
    Integer totalByWaiterAndDate(@Param("id_waiter") Integer id_waiter, @Param("start_date") Date start_date, @Param("end_date") Date end_date);

    @Query("SELECT coalesce(sum(cost), 0) FROM Details WHERE id_invoice IN " +
            "(SELECT id_invoice FROM Invoices WHERE id_client = :id_client)")
    Integer totalByClient(@Param("id_client") Integer id_client);
}
