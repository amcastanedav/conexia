package org.acastane.conexia.repository;

import org.acastane.conexia.core.Details;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by angelica on 29/03/19.
 */
public interface DetailsRepository extends CrudRepository<Details, Integer> {

    @Query("SELECT d FROM Details d WHERE id_invoice = :id_invoice")
    List<Details> getDetailsByInvoice(@Param("id_invoice") Integer id_invoice);
}
