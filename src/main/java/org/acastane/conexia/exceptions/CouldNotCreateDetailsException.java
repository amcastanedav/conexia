package org.acastane.conexia.exceptions;

/**
 * Created by angelica on 29/03/19.
 */
public class CouldNotCreateDetailsException extends Exception {
    public CouldNotCreateDetailsException(String message) {
        super(message);
    }
}
