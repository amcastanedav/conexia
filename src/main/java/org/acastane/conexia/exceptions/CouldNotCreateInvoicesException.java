package org.acastane.conexia.exceptions;

/**
 * Created by angelica on 29/03/19.
 */
public class CouldNotCreateInvoicesException extends Exception {
    public CouldNotCreateInvoicesException(String message) {
        super(message);
    }
}
