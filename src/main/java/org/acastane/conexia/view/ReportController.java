package org.acastane.conexia.view;

import org.acastane.conexia.core.Clients;
import org.acastane.conexia.core.Waiters;
import org.acastane.conexia.service.ConexiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by angelica on 30/03/19.
 */
@Controller
public class ReportController {

    @Autowired
    private ConexiaService conexiaService;

    @GetMapping("/report/waiters")
    public String viewReportByWaiters(@RequestParam(name = "months", required = false, defaultValue = "3") Integer months, Model model) {
        Map<Waiters, Map<String, Integer>> waitersReport = conexiaService.buildReportByWaiters(months);
        Map<String, Map<String, Integer>> report = new HashMap<>();
        for (Map.Entry<Waiters, Map<String, Integer>> entry: waitersReport.entrySet()) {
            Waiters waiter = entry.getKey();
            report.put(waiter.getFirst_name() + " " + waiter.getLast_name1() + " " + waiter.getLast_name2(), entry.getValue());
        }
        model.addAttribute("report", report);
        return "reports/waiters";
    }

    @GetMapping("/report/clients")
    public String viewReportByClients(@RequestParam(name = "limit", required = false, defaultValue = "0") Integer limit, Model model) {
        Map<Clients, Integer> clientsReport = conexiaService.buildReportByClients(limit);
        Map<String, Integer> report = new HashMap<>();
        for (Map.Entry<Clients, Integer> entry:clientsReport.entrySet()) {
            Clients client = entry.getKey();
            report.put(client.getFirst_name() + " " + client.getLast_name1() + " " + client.getLast_name2(), entry.getValue());
        }
        model.addAttribute("report", report);
        model.addAttribute("consumption", limit);
        return "reports/clients";
    }
}
