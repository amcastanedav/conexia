package org.acastane.conexia.view;

import org.acastane.conexia.core.Chefs;
import org.acastane.conexia.core.Clients;
import org.acastane.conexia.core.Tables;
import org.acastane.conexia.core.Waiters;
import org.acastane.conexia.dto.Detail;
import org.acastane.conexia.dto.Invoice;
import org.acastane.conexia.exceptions.CouldNotCreateDetailsException;
import org.acastane.conexia.exceptions.CouldNotCreateInvoicesException;
import org.acastane.conexia.service.ConexiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * Created by angelica on 30/03/19.
 */
@Controller
public class CreateController {

    @Autowired
    private ConexiaService conexiaService;

    @GetMapping("/create/client")
    public String showClientForm(Clients client) {
        return "create/client";
    }

    @PostMapping("/create/client")
    public String createClient(Clients client, Model model) {
        conexiaService.saveClient(client);
        model.addAttribute("message", "A new client has been created");
        return "created";
    }

    @GetMapping("/create/chef")
    public String showChefForm(Chefs chef) {
        return "create/chef";
    }

    @PostMapping("/create/chef")
    public String createChef(Chefs chef, Model model) {
        conexiaService.saveChef(chef);
        model.addAttribute("message", "A new chef has been created");
        return "created";
    }

    @GetMapping("/create/waiter")
    public String showWaiterForm(Waiters waiter) {
        return "create/waiter";
    }

    @PostMapping("/create/waiter")
    public String createWaiter(Waiters waiter, Model model) {
        conexiaService.saveWaiters(waiter);
        model.addAttribute("message", "A new waiter has been created");
        return "created";
    }

    @GetMapping("/create/table")
    public String showTableForm(Tables table) {
        return "create/table";
    }

    @PostMapping("/create/table")
    public String createTable(Tables table, Model model) {
        conexiaService.saveTables(table);
        model.addAttribute("message", "A new table has been created");
        return "created";
    }

    @GetMapping("/create/detail")
    public String showDetailForm(Detail detail) {
        return "create/detail";
    }

    @PostMapping("/create/detail")
    public String createDetail(Detail detail, Model model) {
        try {
            conexiaService.saveDetails(detail.getId_invoice(), detail.getId_chef(), detail.getDish(), detail.getCost());
            model.addAttribute("message", "A new detail has been created");
            return "created";
        } catch (CouldNotCreateDetailsException e) {
            return "create/detail";
        }
    }

    @GetMapping("/create/invoice")
    public String showInvoiceForm(Invoice invoice) {
        return "create/invoice";
    }

    @PostMapping("/create/invoice")
    public String createInvoice(Invoice invoice, Model model) {
        try {
            conexiaService.saveInvoices(invoice.getId_client(), invoice.getId_waiter(), invoice.getId_table());
            model.addAttribute("message", "A new invoice has been created");
            return "created";
        } catch (CouldNotCreateInvoicesException e) {
            return "create/invoice";
        }
    }
}
