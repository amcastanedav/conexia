package org.acastane.conexia.view;

import org.acastane.conexia.core.*;
import org.acastane.conexia.service.ConexiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

/**
 * Created by angelica on 30/03/19.
 */
@Controller
public class ViewController {

    @Autowired
    private ConexiaService conexiaService;

    @GetMapping("/view/client")
    public String viewClient(@RequestParam(name = "id", required = true) Integer id, Model model) {
        Optional<Clients> clients = conexiaService.getClients(id);
        if (clients.isPresent()) {
            model.addAttribute("client", clients.get());
            return "view/client";
        } else {
            model.addAttribute("message", "The client you are looking for was not found");
            return "notFound";
        }
    }

    @GetMapping("/view/chef")
    public String viewChef(@RequestParam(name = "id", required = true) Integer id, Model model) {
        Optional<Chefs> chefs = conexiaService.getChefs(id);
        if (chefs.isPresent()) {
            model.addAttribute("chef", chefs.get());
            return "view/chef";
        } else {
            model.addAttribute("message", "The chef you are looking for was not found");
            return "notFound";
        }
    }

    @GetMapping("/view/waiter")
    public String viewWaiter(@RequestParam(name = "id", required = true) Integer id, Model model) {
        Optional<Waiters> waiters = conexiaService.getWaiters(id);
        if (waiters.isPresent()) {
            model.addAttribute("waiter", waiters.get());
            return "view/waiter";
        } else {
            model.addAttribute("message", "The waiter you are looking for was not found");
            return "notFound";
        }
    }

    @GetMapping("/view/table")
    public String viewTable(@RequestParam(name = "id", required = true) Integer id, Model model) {
        Optional<Tables> tables = conexiaService.getTables(id);
        if (tables.isPresent()) {
            model.addAttribute("table", tables.get());
            return "view/table";
        } else {
            model.addAttribute("message", "The table you are looking for was not found");
            return "notFound";
        }
    }

    @GetMapping("/view/invoice")
    public String viewInvoice(@RequestParam(name = "id", required = true) Integer id, Model model) {
        Optional<Invoices> invoices = conexiaService.getInvoices(id);
        if (invoices.isPresent()) {
            Clients clients = invoices.get().getClient();
            Waiters waiter = invoices.get().getWaiter();
            List<Details> detailsList = conexiaService.getDetailsByInvoice(invoices.get().getId_invoice());
            model.addAttribute("id_invoice", invoices.get().getId_invoice());
            model.addAttribute("client_name", clients.getFirst_name() + " " + clients.getLast_name1() + " " + clients.getLast_name2());
            model.addAttribute("waiter_name", waiter.getFirst_name() + " " + waiter.getLast_name1() + " " + waiter.getLast_name2());
            model.addAttribute("id_table", invoices.get().getTable().getId_table());
            model.addAttribute("date", invoices.get().getDate_invoice());
            model.addAttribute("details", detailsList);
            model.addAttribute("total", detailsList.stream().map(Details::getCost).mapToInt(Integer::valueOf).sum());
            return "view/invoice";
        } else {
            model.addAttribute("message", "The invoice you are looking for was not found");
            return "notFound";
        }
    }

    @GetMapping("/view/detail")
    public String viewDetail(@RequestParam(name = "id", required = true) Integer id, Model model) {
        Optional<Details> details = conexiaService.getDetails(id);
        if (details.isPresent()) {
            Clients clients = details.get().getInvoices().getClient();
            Chefs chefs = details.get().getChefs();
            model.addAttribute("detail", details.get());
            model.addAttribute("id_invoice", details.get().getInvoices().getId_invoice());
            model.addAttribute("client_name", clients.getFirst_name() + " " + clients.getLast_name1() + " " + clients.getLast_name2());
            model.addAttribute("chef_name", chefs.getFirst_name() + " " + chefs.getLast_name1() + " " + chefs.getLast_name2());
            return "view/detail";
        } else {
            model.addAttribute("message", "The detail you are looking for was not found");
            return "notFound";
        }
    }
}
