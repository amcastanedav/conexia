package org.acastane.conexia.core;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by angelica on 29/03/19.
 */
@Entity
public class Waiters {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_waiter;

    private String first_name;

    private String last_name1;

    private String last_name2;

    public Waiters() {
    }

    public Waiters(String first_name, String last_name1, String last_name2) {
        this.first_name = first_name;
        this.last_name1 = last_name1;
        this.last_name2 = last_name2;
    }

    public Integer getId_waiter() {
        return id_waiter;
    }

    public void setId_waiter(Integer id_waiter) {
        this.id_waiter = id_waiter;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name1() {
        return last_name1;
    }

    public void setLast_name1(String last_name1) {
        this.last_name1 = last_name1;
    }

    public String getLast_name2() {
        return last_name2;
    }

    public void setLast_name2(String last_name2) {
        this.last_name2 = last_name2;
    }

    @Override
    public String toString() {
        return "Waiters{" +
                "id_waiter=" + id_waiter +
                ", first_name='" + first_name + '\'' +
                ", last_name1='" + last_name1 + '\'' +
                ", last_name2='" + last_name2 + '\'' +
                '}';
    }
}
