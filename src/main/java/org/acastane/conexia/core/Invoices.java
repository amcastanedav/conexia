package org.acastane.conexia.core;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by angelica on 29/03/19.
 */
@Entity
public class Invoices {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_invoice;

    @ManyToOne
    @JoinColumn(name="id_client", nullable=false)
    private Clients client;

    @ManyToOne
    @JoinColumn(name="id_waiter", nullable=false)
    private Waiters waiter;

    @ManyToOne
    @JoinColumn(name="id_table", nullable=false)
    private Tables table;

    private Date date_invoice;

    public Invoices() {
    }

    public Invoices(Clients client, Waiters waiter, Tables table, Date date_invoice) {
        this.client = client;
        this.waiter = waiter;
        this.table = table;
        this.date_invoice = date_invoice;
    }

    public Integer getId_invoice() {
        return id_invoice;
    }

    public void setId_invoice(Integer id_invoice) {
        this.id_invoice = id_invoice;
    }

    public Clients getClient() {
        return client;
    }

    public void setClient(Clients client) {
        this.client = client;
    }

    public Waiters getWaiter() {
        return waiter;
    }

    public void setWaiter(Waiters waiter) {
        this.waiter = waiter;
    }

    public Tables getTable() {
        return table;
    }

    public void setTable(Tables table) {
        this.table = table;
    }

    public Date getDate_invoice() {
        return date_invoice;
    }

    public void setDate_invoice(Date date_invoice) {
        this.date_invoice = date_invoice;
    }

    @Override
    public String toString() {
        return "Invoices{" +
                "id_invoice=" + id_invoice +
                ", client=" + client.toString() +
                ", waiter=" + waiter.toString() +
                ", table=" + table.toString() +
                ", date_invoice=" + date_invoice +
                '}';
    }
}
