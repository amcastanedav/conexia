package org.acastane.conexia.core;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by angelica on 29/03/19.
 */
@Entity
public class Clients {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_client;

    private String first_name;

    private String last_name1;

    private String last_name2;

    private String observations;

    public Clients() {
    }

    public Clients(String first_name, String last_name1, String last_name2, String observations) {
        this.first_name = first_name;
        this.last_name1 = last_name1;
        this.last_name2 = last_name2;
        this.observations = observations;
    }

    public Integer getId_client() {
        return id_client;
    }

    public void setId_client(Integer id_client) {
        this.id_client = id_client;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name1() {
        return last_name1;
    }

    public void setLast_name1(String last_name1) {
        this.last_name1 = last_name1;
    }

    public String getLast_name2() {
        return last_name2;
    }

    public void setLast_name2(String last_name2) {
        this.last_name2 = last_name2;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    @Override
    public String toString() {
        return "Clients{" +
                "id_client=" + id_client +
                ", first_name='" + first_name + '\'' +
                ", last_name1='" + last_name1 + '\'' +
                ", last_name2='" + last_name2 + '\'' +
                ", observations='" + observations + '\'' +
                '}';
    }
}
