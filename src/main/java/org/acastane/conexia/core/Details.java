package org.acastane.conexia.core;

import javax.persistence.*;

/**
 * Created by angelica on 29/03/19.
 */
@Entity
public class Details {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_detail;

    @ManyToOne
    @JoinColumn(name="id_invoice", nullable=false)
    private Invoices invoices;

    @ManyToOne
    @JoinColumn(name="id_chef", nullable=false)
    private Chefs chefs;

    private String dish;

    private Integer cost;

    public Details() {
    }

    public Details(Invoices invoices, Chefs chefs, String dish, Integer cost) {
        this.invoices = invoices;
        this.chefs = chefs;
        this.dish = dish;
        this.cost = cost;
    }

    public Integer getId_detail() {
        return id_detail;
    }

    public void setId_detail(Integer id_detail) {
        this.id_detail = id_detail;
    }

    public Invoices getInvoices() {
        return invoices;
    }

    public void setInvoices(Invoices invoices) {
        this.invoices = invoices;
    }

    public Chefs getChefs() {
        return chefs;
    }

    public void setChefs(Chefs chefs) {
        this.chefs = chefs;
    }

    public String getDish() {
        return dish;
    }

    public void setDish(String dish) {
        this.dish = dish;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "Details{" +
                "id_detail=" + id_detail +
                ", invoices=" + invoices.toString() +
                ", chefs=" + chefs.toString() +
                ", dish='" + dish + '\'' +
                ", cost=" + cost +
                '}';
    }
}
