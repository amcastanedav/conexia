package org.acastane.conexia.core;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by angelica on 29/03/19.
 */
@Entity
public class Chefs {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_chef;

    private String first_name;

    private String last_name1;

    private String last_name2;

    public Chefs() {
    }

    public Chefs(String first_name, String last_name1, String last_name2) {
        this.first_name = first_name;
        this.last_name1 = last_name1;
        this.last_name2 = last_name2;
    }

    public Integer getId_chef() {
        return id_chef;
    }

    public void setId_chef(Integer id_chef) {
        this.id_chef = id_chef;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name1() {
        return last_name1;
    }

    public void setLast_name1(String last_name1) {
        this.last_name1 = last_name1;
    }

    public String getLast_name2() {
        return last_name2;
    }

    public void setLast_name2(String last_name2) {
        this.last_name2 = last_name2;
    }

    @Override
    public String toString() {
        return "Chefs{" +
                "id_chef=" + id_chef +
                ", first_name='" + first_name + '\'' +
                ", last_name1='" + last_name1 + '\'' +
                ", last_name2='" + last_name2 + '\'' +
                '}';
    }
}
