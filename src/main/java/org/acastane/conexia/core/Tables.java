package org.acastane.conexia.core;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by angelica on 29/03/19.
 */
@Entity
public class Tables {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_table;

    private Integer max_num;

    private String location;

    public Tables() {
    }

    public Tables(Integer max_num, String location) {
        this.max_num = max_num;
        this.location = location;
    }

    public Integer getId_table() {
        return id_table;
    }

    public void setId_table(Integer id_table) {
        this.id_table = id_table;
    }

    public Integer getMax_num() {
        return max_num;
    }

    public void setMax_num(Integer max_num) {
        this.max_num = max_num;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "Tables{" +
                "id_table=" + id_table +
                ", max_num=" + max_num +
                ", location='" + location + '\'' +
                '}';
    }
}
