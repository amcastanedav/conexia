package org.acastane.conexia.service;

import org.acastane.conexia.core.*;
import org.acastane.conexia.exceptions.CouldNotCreateDetailsException;
import org.acastane.conexia.exceptions.CouldNotCreateInvoicesException;
import org.acastane.conexia.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by angelica on 29/03/19.
 */
@Service
public class ConexiaService {

    @Autowired
    private ClientsRepository clientsRepository;

    @Autowired
    private ChefsRepository chefsRepository;

    @Autowired
    private TablesRepository tablesRepository;

    @Autowired
    private WaitersRepository waitersRepository;

    @Autowired
    private InvoicesRepository invoicesRepository;

    @Autowired
    private DetailsRepository detailsRepository;

    private Calendar calendar = Calendar.getInstance();

    public Optional<Clients> getClients(Integer id){
        return clientsRepository.findById(id);
    }

    public void saveClient(Clients clients) {
        clientsRepository.save(clients);
    }

    public Optional<Chefs> getChefs(Integer id){
        return chefsRepository.findById(id);
    }

    public void saveChef(Chefs chefs){
        chefsRepository.save(chefs);
    }

    public Optional<Tables> getTables(Integer id){
        return tablesRepository.findById(id);
    }

    public void saveTables(Tables tables){
        tablesRepository.save(tables);
    }

    public Optional<Waiters> getWaiters(Integer id){
        return waitersRepository.findById(id);
    }

    public void saveWaiters(Waiters waiters) {
        waitersRepository.save(waiters);
    }

    public Optional<Invoices> getInvoices(Integer id){
        return invoicesRepository.findById(id);
    }

    public void saveInvoices(Integer id_client, Integer id_waiter, Integer id_table) throws CouldNotCreateInvoicesException {
        Optional<Clients> clients = clientsRepository.findById(id_client);
        Optional<Waiters> waiters = waitersRepository.findById(id_waiter);
        Optional<Tables> tables = tablesRepository.findById(id_table);

        if (clients.isPresent() && waiters.isPresent() && tables.isPresent()){
            Invoices invoices = new Invoices(clients.get(), waiters.get(), tables.get(), new Date());
            invoicesRepository.save(invoices);
        } else {
            throw new CouldNotCreateInvoicesException("Invoices could not be created");
        }
    }

    public Optional<Details> getDetails(Integer id){
        return detailsRepository.findById(id);
    }

    public List<Details> getDetailsByInvoice(Integer id) {
        return detailsRepository.getDetailsByInvoice(id);
    }

    public void saveDetails(Integer id_invoice, Integer id_chef, String dish, Integer cost) throws CouldNotCreateDetailsException {
        Optional<Invoices> invoices = invoicesRepository.findById(id_invoice);
        Optional<Chefs> chefs = chefsRepository.findById(id_chef);

        if (invoices.isPresent() && chefs.isPresent()) {
            Details details = new Details(invoices.get(), chefs.get(), dish, cost);
            detailsRepository.save(details);
        } else {
            throw new CouldNotCreateDetailsException("Details could not be created");
        }
    }

    public Map<Waiters, Map<String, Integer>> buildReportByWaiters(Integer num_monts){
        HashMap<Waiters, Map<String, Integer>> result = new HashMap<>();

        Iterator<Waiters> waitersIterator = waitersRepository.findAll().iterator();
        Map<Date, Date> dateMap = lastMonths(num_monts);

        while (waitersIterator.hasNext()){
            Waiters waiters = waitersIterator.next();
            Map<String, Integer> totalMap = new HashMap<>();
            for (Map.Entry<Date, Date> dateEntry:dateMap.entrySet()){
                calendar.setTime(dateEntry.getKey());
                totalMap.put(calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()), totalByWaiterAndDate(waiters.getId_waiter(), dateEntry.getKey(), dateEntry.getValue()));
            }
            result.put(waiters, totalMap);
        }

        return result;
    }

    public Map<Clients, Integer> buildReportByClients(Integer limit){
        HashMap<Clients, Integer> result = new HashMap<>();

        Iterator<Clients> clientsIterator = clientsRepository.findAll().iterator();

        while (clientsIterator.hasNext()){
            Clients clients = clientsIterator.next();
            Integer consumption = invoicesRepository.totalByClient(clients.getId_client());
            if (consumption > limit){
                result.put(clients,consumption);
            }
        }

        return result;
    }

    private Integer totalByWaiterAndDate(Integer id_waiter, Date start_date, Date end_date){
        return invoicesRepository.totalByWaiterAndDate(id_waiter, start_date, end_date);
    }

    protected Map<Date, Date> lastMonths(Integer num_months){
        HashMap<Date, Date> dateMap = new HashMap<>();

        calendar.setTimeInMillis(System.currentTimeMillis());

        for (int i=0; i < num_months; i++){
            Date end = getEnd();
            Date start = getStart();
            dateMap.put(start, end);
            calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
        }

        return dateMap;
    }

    private Date getStart(){
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return new Date(calendar.getTimeInMillis());
    }

    private Date getEnd(){
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 0);
        return new Date(calendar.getTimeInMillis());
    }
}
