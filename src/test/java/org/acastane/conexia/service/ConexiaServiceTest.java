package org.acastane.conexia.service;

import org.acastane.conexia.core.*;
import org.acastane.conexia.exceptions.CouldNotCreateDetailsException;
import org.acastane.conexia.exceptions.CouldNotCreateInvoicesException;
import org.acastane.conexia.repository.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static org.mockito.BDDMockito.given;

/**
 * Created by angelica on 29/03/19.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ConexiaServiceTest {

    @MockBean
    private ClientsRepository clientsRepository;

    @MockBean
    private ChefsRepository chefsRepository;

    @MockBean
    private TablesRepository tablesRepository;

    @MockBean
    private WaitersRepository waitersRepository;

    @MockBean
    private InvoicesRepository invoicesRepository;

    @MockBean
    private DetailsRepository detailsRepository;

    @Autowired
    private ConexiaService conexiaService;

    private Random random = new Random();

    @Test(expected = CouldNotCreateInvoicesException.class)
    public void whenClientDoesNotExistInvoiceShouldNotBeCreated() throws CouldNotCreateInvoicesException {
        given(clientsRepository.findById(1)).willReturn(Optional.empty());
        given(waitersRepository.findById(1)).willReturn(Optional.of(new Waiters()));
        given(tablesRepository.findById(1)).willReturn(Optional.of(new Tables()));

        conexiaService.saveInvoices(1, 1, 1);
    }

    @Test(expected = CouldNotCreateInvoicesException.class)
    public void whenWaiterDoesNotExistInvoiceShouldNotBeCreated() throws CouldNotCreateInvoicesException {
        given(clientsRepository.findById(1)).willReturn(Optional.of(new Clients()));
        given(waitersRepository.findById(1)).willReturn(Optional.empty());
        given(tablesRepository.findById(1)).willReturn(Optional.of(new Tables()));

        conexiaService.saveInvoices(1, 1, 1);
    }

    @Test(expected = CouldNotCreateInvoicesException.class)
    public void whenTableDoesNotExistInvoiceShouldNotBeCreated() throws CouldNotCreateInvoicesException {
        given(clientsRepository.findById(1)).willReturn(Optional.of(new Clients()));
        given(waitersRepository.findById(1)).willReturn(Optional.of(new Waiters()));
        given(tablesRepository.findById(1)).willReturn(Optional.empty());

        conexiaService.saveInvoices(1, 1, 1);
    }

    @Test(expected = CouldNotCreateDetailsException.class)
    public void whenInvoiceDoesntExistDetailShouldNotBeCreated() throws CouldNotCreateDetailsException {
        given(invoicesRepository.findById(1)).willReturn(Optional.empty());
        given(chefsRepository.findById(1)).willReturn(Optional.of(new Chefs()));

        conexiaService.saveDetails(1, 1, "dish", 1000);
    }

    @Test(expected = CouldNotCreateDetailsException.class)
    public void whenChefDoesntExistDetailShouldNotBeCreated() throws CouldNotCreateDetailsException {
        given(invoicesRepository.findById(1)).willReturn(Optional.of(new Invoices()));
        given(chefsRepository.findById(1)).willReturn(Optional.empty());

        conexiaService.saveDetails(1, 1, "dish", 1000);
    }

    @Test
    public void whenDataShouldBuildReportByWaiters(){
        Waiters waiters1 = new Waiters("carlos", "gomez", "perez");
        waiters1.setId_waiter(1);
        Waiters waiters2 = new Waiters("camilo", "giraldo", "mendez");
        waiters2.setId_waiter(2);

        given(waitersRepository.findAll()).willReturn(Arrays.asList(waiters1,waiters2));

        Map<Date, Date> dateMap = conexiaService.lastMonths(3);
        for (Map.Entry<Date, Date> dateEntry:dateMap.entrySet()){
            given(invoicesRepository.totalByWaiterAndDate(1, dateEntry.getKey(), dateEntry.getValue())).willReturn(random.nextInt(10000));
            given(invoicesRepository.totalByWaiterAndDate(2, dateEntry.getKey(), dateEntry.getValue())).willReturn(random.nextInt(10000));
        }

        Map<Waiters, Map<String, Integer>> result = conexiaService.buildReportByWaiters(3);

        Assert.assertEquals(2, result.size());
        Assert.assertEquals(3, result.get(waiters1).size());
        Assert.assertEquals(3, result.get(waiters2).size());
        Assert.assertTrue(result.get(waiters1).values().stream().mapToInt(Integer::intValue).sum() > 0);
        Assert.assertTrue(result.get(waiters2).values().stream().mapToInt(Integer::intValue).sum() > 0);
    }

    @Test
    public void whenDataShouldBuildReportByClients(){
        Clients clients1 = new Clients("oscar", "ariza", "pardo", "le gusta el arroz");
        clients1.setId_client(1);
        Clients clients2 = new Clients("manuel", "blanco", "pardo", "le gusta la pasta");
        clients2.setId_client(2);
        Clients clients3 = new Clients("esteban", "negro", "pardo", "le gusta el queso");
        clients3.setId_client(3);

        given(clientsRepository.findAll()).willReturn(Arrays.asList(clients1,clients2, clients3));
        given(invoicesRepository.totalByClient(1)).willReturn(random.nextInt(10000));
        given(invoicesRepository.totalByClient(2)).willReturn(random.nextInt(10000));
        given(invoicesRepository.totalByClient(3)).willReturn(random.nextInt(10000));

        Map<Clients, Integer> result = conexiaService.buildReportByClients(0);

        Assert.assertEquals(3, result.size());
        Assert.assertTrue(result.get(clients1) > 0);
        Assert.assertTrue(result.get(clients2) > 0);
        Assert.assertTrue(result.get(clients3) > 0);
    }
}
