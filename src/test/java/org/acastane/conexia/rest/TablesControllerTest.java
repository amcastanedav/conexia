package org.acastane.conexia.rest;

import org.acastane.conexia.core.Tables;
import org.acastane.conexia.service.ConexiaService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by angelica on 29/03/19.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(TablesController.class)
public class TablesControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ConexiaService conexiaService;

    @Test
    public void whenObjectExistsThenIsReturned() throws Exception {
        given(conexiaService.getTables(1)).willReturn(Optional.of(new Tables()));

        mvc.perform(get("/tables/1"))
                .andExpect(status().isOk());
    }

    @Test
    public void whenObjectDoesntExistThenIsNotReturned() throws Exception {
        given(conexiaService.getTables(2)).willReturn(Optional.empty());

        mvc.perform(get("/tables/2"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void whenRequestHasMissingParametersThenObjectIsNotCreated () throws Exception {
        mvc.perform(post("/tables/create").contentType(MediaType.APPLICATION_JSON).content(""))
                .andExpect(status().isBadRequest());

        mvc.perform(post("/tables/create").contentType(MediaType.APPLICATION_JSON).content("{" +
                "\"location\" : \"jardin\"" +
                "}"))
                .andExpect(status().isBadRequest());

        mvc.perform(post("/tables/create").contentType(MediaType.APPLICATION_JSON).content("{" +
                "\"max_num\" : \"8\"," +
                "}"))
                .andExpect(status().isBadRequest());

        mvc.perform(post("/tables/create").contentType(MediaType.APPLICATION_JSON).content("{" +
                "\"max_num\" : \"ocho\"," +
                "\"location\" : \"jardin\"" +
                "}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenRequestIsOkThenObjectShouldBeCreated() throws Exception {
        mvc.perform(post("/tables/create").contentType(MediaType.APPLICATION_JSON).content("{" +
                "\"max_num\" : \"8\"," +
                "\"location\" : \"jardin\"" +
                "}"))
                .andExpect(status().isOk());
    }
}
