package org.acastane.conexia.rest;

import org.acastane.conexia.core.Clients;
import org.acastane.conexia.service.ConexiaService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by angelica on 29/03/19.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(ClientsController.class)
public class ClientsControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ConexiaService conexiaService;

    @Test
    public void whenObjectExistsThenIsReturned() throws Exception {
        given(conexiaService.getClients(1)).willReturn(Optional.of(new Clients()));

        mvc.perform(get("/clients/1"))
                .andExpect(status().isOk());
    }

    @Test
    public void whenObjectDoesntExistThenIsNotReturned() throws Exception {
        given(conexiaService.getClients(2)).willReturn(Optional.empty());

        mvc.perform(get("/clients/2"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void whenRequestHasMissingParametersThenObjectIsNotCreated () throws Exception {
        mvc.perform(post("/clients/create").contentType(MediaType.APPLICATION_JSON).content(""))
                .andExpect(status().isBadRequest());

        mvc.perform(post("/clients/create").contentType(MediaType.APPLICATION_JSON).content("{" +
                "\"last_name1\" : \"perez\"," +
                "\"last_name2\" : \"gomez\"," +
                "\"observations\" : \"le gusta la carne\"" +
                "}"))
                .andExpect(status().isBadRequest());

        mvc.perform(post("/clients/create").contentType(MediaType.APPLICATION_JSON).content("{" +
                "\"first_name\" : \"pepito\"," +
                "\"last_name2\" : \"gomez\"," +
                "\"observations\" : \"le gusta la carne\"" +
                "}"))
                .andExpect(status().isBadRequest());

        mvc.perform(post("/clients/create").contentType(MediaType.APPLICATION_JSON).content("{" +
                "\"first_name\" : \"pepito\"," +
                "\"last_name1\" : \"perez\"," +
                "\"observations\" : \"le gusta la carne\"" +
                "}"))
                .andExpect(status().isBadRequest());

        mvc.perform(post("/clients/create").contentType(MediaType.APPLICATION_JSON).content("{" +
                "\"first_name\" : \"pepito\"," +
                "\"last_name1\" : \"perez\"," +
                "\"last_name2\" : \"gomez\"," +
                "}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenRequestIsOkThenObjectShouldBeCreated() throws Exception {
        mvc.perform(post("/clients/create").contentType(MediaType.APPLICATION_JSON).content("{" +
                "\"first_name\" : \"pepito\"," +
                "\"last_name1\" : \"perez\"," +
                "\"last_name2\" : \"gomez\"," +
                "\"observations\" : \"le gusta la carne\"" +
                "}"))
                .andExpect(status().isOk());
    }
}
