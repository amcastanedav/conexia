package org.acastane.conexia.rest;

import org.acastane.conexia.core.Chefs;
import org.acastane.conexia.service.ConexiaService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by angelica on 29/03/19.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(ChefsController.class)
public class ChefsControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ConexiaService conexiaService;

    @Test
    public void whenObjectExistsThenIsReturned() throws Exception {
        given(conexiaService.getChefs(1)).willReturn(Optional.of(new Chefs()));

        mvc.perform(get("/chefs/1"))
                .andExpect(status().isOk());
    }

    @Test
    public void whenObjectDoesntExistThenIsNotReturned() throws Exception {
        given(conexiaService.getChefs(2)).willReturn(Optional.empty());

        mvc.perform(get("/chefs/2"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void whenRequestHasMissingParametersThenObjectIsNotCreated () throws Exception {
        mvc.perform(post("/chefs/create").contentType(MediaType.APPLICATION_JSON).content(""))
                .andExpect(status().isBadRequest());

        mvc.perform(post("/chefs/create").contentType(MediaType.APPLICATION_JSON).content("{" +
                "\"last_name1\" : \"perez\"," +
                "\"last_name2\" : \"gomez\"" +
                "}"))
                .andExpect(status().isBadRequest());

        mvc.perform(post("/chefs/create").contentType(MediaType.APPLICATION_JSON).content("{" +
                "\"first_name\" : \"pepito\"," +
                "\"last_name2\" : \"gomez\"" +
                "}"))
                .andExpect(status().isBadRequest());

        mvc.perform(post("/chefs/create").contentType(MediaType.APPLICATION_JSON).content("{" +
                "\"first_name\" : \"pepito\"," +
                "\"last_name1\" : \"perez\"," +
                "}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenRequestIsOkThenObjectShouldBeCreated() throws Exception {
        mvc.perform(post("/chefs/create").contentType(MediaType.APPLICATION_JSON).content("{" +
                "\"first_name\" : \"pepito\"," +
                "\"last_name1\" : \"perez\"," +
                "\"last_name2\" : \"gomez\"" +
                "}"))
                .andExpect(status().isOk());
    }
}
